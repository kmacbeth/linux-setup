# Linux Setup

packages/packages.list: list of packages to install

## Generate the packages.list

bin/build\_package\_list.sh > packages/packages.list

## Install from packages/packages.list

bin/install\_package\_list.sh packages/packages.list

